Pod::Spec.new do |s|

# --
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "BillingOptionsStyleKit"
s.summary = "BillingOptionsStyleKit provides drawing methods for billing option assets."
s.requires_arc = true

# --
s.version = "1.0.2"

# --
s.license = { :type => "Private", :file => "LICENSE" }

# --
s.author = { "Christopher Miller" => "christopher.miller1@one.verizon.com" }

# --
s.homepage = "https://bitbucket.org/maxintegerinc/billingoptionsstylekit"

# --
s.source = { :git => "https://bitbucket.org/maxintegerinc/billingoptionsstylekit.git", :tag => "#{s.version}"}

# --
s.framework = "UIKit"
#s.dependency 'Alamofire', '~> 2.0'
#s.dependency 'MBProgressHUD', '~> 0.9.0'

# --
s.source_files = "BillingOptionsStyleKit/**/*.{swift}"

# --
s.resources = "BillingOptionsStyleKit/**/*.{png,jpeg,jpg,storyboard,xib}"

end